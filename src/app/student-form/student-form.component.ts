import { AuthService } from './../auth.service';
import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Student } from '../interfaces/student';
import { PredictionService } from '../prediction.service';
import { StudentsService } from '../students.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {
  predictClick:boolean = false;

  math: number;
  sat: number;
  pay: boolean = false;
  result:string;
  id: string;
  selectedOption:boolean;
  options = [{value:false,viewValue:'will not drop'},{value:true,viewValue:'will drop'}];
  email:string;
  predict(){
    this.predictionService.predict(this.math,this.sat,this.pay).subscribe(
      res => {
        console.log(res);
        if(res > 0.5){
          var result = 'Will not drop'
          this.selectedOption = false;
        } else {
          console.log('here');
          var result = 'Will drop'
          this.selectedOption = true;
        }
        this.result = result}
    )
  }
  addStudent(){
    this.StudentsService.addStudent(this.math,this.sat,this.pay,this.result,this.email).subscribe(
      res => this.router.navigate(['/students'])
    )
  }




  constructor(private router:Router,private StudentsService: StudentsService, private authService:AuthService,public predictionService:PredictionService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user =>this.email = user.email
    )
  }

}
