import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {e
  private url = "https://q4o00zm87l.execute-api.us-east-1.amazonaws.com/test-hagasha";

  predict(math:number, sat:number, pay:boolean):Observable<any> {
    let json = {
      "data": {
        "math": math,
        "sat": sat,
        "pay": pay
      }
    }
    

    let body = JSON.stringify(json); //text to input in internet
    console.log("in predict");
    return this.http.post<any>(this.url, body).pipe(
      map(res => {
        console.log(res);
        console.log("in the map"); 
        console.log(res);
        return res.body;      
      })
    )
  }

  constructor(private http:HttpClient) { }
}
