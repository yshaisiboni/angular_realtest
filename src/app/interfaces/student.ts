export interface Student {
    math: number;
    sat: number;
    pay: Boolean;
    id?:string;
    saved?:Boolean;
    result?:string;
}
