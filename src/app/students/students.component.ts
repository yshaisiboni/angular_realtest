import { Student } from './../interfaces/student';
import { StudentsService } from './../students.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  students: Student[];
  students$;

  displayedColumns: string[] = ['math', 'sat', 'pay', 'result','email', 'Delete'];

  constructor(private StudentsService: StudentsService, public authService: AuthService) { }

  deleteStudent(sid){
    console.log(sid);
    this.StudentsService.deleteStudent(sid);
  }
  ngOnInit(): void {
    this.students$ = this.StudentsService.getStudents();
    this.students$.subscribe(
      docs => {
        console.log(docs);
        this.students = [];
        var i = 0;
        for (let document of docs) {
          console.log(i++);
          const student: Student = document.payload.doc.data();
          student.id = document.payload.doc.id;
          console.log(document.payload.doc.data());
          this.students.push(student);
        }
      }
    )
    console.log(this.students)
  }
}
