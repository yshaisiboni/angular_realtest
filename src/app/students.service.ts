import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  studentsCollection:AngularFirestoreCollection;

  public getStudents(): Observable<any[]> {
    this.studentsCollection = this.db.collection(`/students`);
    return this.studentsCollection.snapshotChanges();
  }

  deleteStudent(sid){
    this.studentsCollection.doc(sid).delete();
    return this.studentsCollection.snapshotChanges();
  }
  addStudent(math,sat,pay,result,email){
    const student ={
      math,
      sat,
      pay,
      result,
      email
    }
  this.studentsCollection.add(student);
 return  this.studentsCollection.snapshotChanges();
  }

  constructor(private db:AngularFirestore) { }
}
