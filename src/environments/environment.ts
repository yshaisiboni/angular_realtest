// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBV5fLbHQDoK16vUHTcpXphlYa6W92Hx3Q",
    authDomain: "realtest-ishay-311c5.firebaseapp.com",
    projectId: "realtest-ishay-311c5",
    storageBucket: "realtest-ishay-311c5.appspot.com",
    messagingSenderId: "764728832144",
    appId: "1:764728832144:web:4824490df9297d46dba815"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
